﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApp.DataModels
{
	/// <summary>
	/// Студент.
	/// </summary>
	public class Student
	{
		/// <summary>
		/// Идентификатор.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Имя.
		/// </summary>
		[Required]
		[StringLength(30)]
		public string Name { get; set; }

		/// <summary>
		/// Дата рождения.
		/// </summary>
		[DataType(DataType.Date)]
		public DateTime BirthDate { get; set; }

		/// <summary>
		/// Название группы.
		/// </summary>
		[Required]
		public string GroupName { get; set; }

		/// <summary>
		/// Сданные экзамены.
		/// </summary>
		public ICollection<Examination> Examinations { get; set; }
	}
}
