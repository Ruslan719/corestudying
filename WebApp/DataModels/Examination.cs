﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.DataModels
{
	/// <summary>
	/// Данные сдачи экзамена.
	/// </summary>
    public class Examination
    {
		/// <summary>
		/// Идентификатор.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Дата сдачи.
		/// </summary>
		[DataType(DataType.Date)]
		public DateTime Date { get; set; }

		/// <summary>
		/// Оценка.
		/// </summary>
		[Range(1, 5)]
		public byte Grade { get; set; }

		/// <summary>
		/// Название экзамена.
		/// </summary>
		[Required]
		[StringLength(30)]
		public string Name { get; set; }

		/// <summary>
		/// Идентификатор студента.
		/// </summary>
		public int StudentId { get; set; }

		/// <summary>
		/// Студент.
		/// </summary>
		public Student Student { get; set; }
	}
}
