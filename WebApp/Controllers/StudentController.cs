﻿using Microsoft.AspNetCore.Mvc;
using WebApp.Models;
using WebApp.Services;
using Microsoft.Extensions.DependencyInjection;
using WebApp.DbContexts;
using System.Linq;
using DbStudent = WebApp.DataModels.Student;

namespace WebApp.Controllers
{
	public class StudentController : Controller
    {
		/// <summary>
		/// Сервис студентов.
		/// </summary>
		private readonly IStudentService _studentService;

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="student_service">Сервис студентов.</param>
		/// <param name="student_context">Контекст студентов.</param>
		public StudentController(IStudentService student_service)
		{
			_studentService = student_service;
		}

		#region Actions

		/// <summary>
		/// Возвращает представление со списком студентов.
		/// </summary>
		public IActionResult Index() =>
			View("StudentList", _studentService.GetAll().Select(ConvertStudent));

		/// <summary>
		/// Возвращает представление с информацией о студенте.
		/// </summary>
		/// <param name="student_id">Идентификатор студента.</param>
		public IActionResult Details(int student_id) =>
			View("StudentDetails", ConvertStudent(_studentService.Get(student_id)));

		/// <summary>
		/// Добавляет студента.
		/// </summary>
		/// <param name="student">Добавляемый студент.</param>
		[HttpPost]
		public IActionResult Add(Student student)
		{
			if (!ModelState.IsValid)
			{
				return View("StudentEdit", null);
			}
			
			DbStudent db_student = ConvertStudent(student);
			_studentService.Add(db_student);
			return RedirectToAction("Details", new { student_id = db_student.Id });
		}

		/// <summary>
		/// Возвращает представление для добавления студента.
		/// </summary>
		public IActionResult Add() => View("StudentEdit", null);

		/// <summary>
		/// Удаляет студента.
		/// </summary>
		/// <param name="student_id">Идентификатор студента.</param>
		public IActionResult Remove(int student_id)
		{
			_studentService.Remove(student_id);
			return RedirectToAction("Index");
		}

		/// <summary>
		/// Изменяет студента.
		/// </summary>
		/// <param name="student">Отредактированный студент.</param>
		[HttpPost]
		public IActionResult Edit(Student student)
		{
			if (!ModelState.IsValid)
			{
				return View("StudentEdit", student);
			}

			_studentService.Edit(ConvertStudent(student));
			return RedirectToAction("Details", new { student_id = student.Id });
		}

		/// <summary>
		/// Возвращает представление для изменения студента.
		/// </summary>
		/// <param name="student_id">Идентификатор студента.</param>
		public IActionResult Edit(int student_id) =>
			View("StudentEdit", ConvertStudent(_studentService.Get(student_id)));

		#endregion

		#region Methods/Private

		/// <summary>
		/// Преобразует студента.
		/// </summary>
		private DbStudent ConvertStudent(Student student)
		{
			return new DbStudent
			{
				Id = student.Id ?? 0,
				Name = student.Name,
				BirthDate = student.BirthDate,
				GroupName = student.GroupName
			};
		}

		/// <summary>
		/// Преобразует студента.
		/// </summary>
		private Student ConvertStudent(DbStudent student)
		{
			return new Student
			{
				Id = student.Id,
				Name = student.Name,
				BirthDate = student.BirthDate,
				GroupName = student.GroupName,
				ExaminationNames = student.Examinations?
					.Select(e => e.Name)
					.ToList()
			};
		}

		#endregion
	}
}