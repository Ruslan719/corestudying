﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebApp.Services;
using WebApp.DbContexts;
using WebApp.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApp
{
    public class Startup
    {
		private const string CONNECTION_STRING = "Server=(LocalDb)\\MSSQLLocalDB;Database=CoreStudying2;Trusted_Connection=True;MultipleActiveResultSets=true;";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
			services.AddScoped<IStudentService, StudentDbService>();
			services.AddDbContext<StudentContext>(options => options.UseSqlServer(CONNECTION_STRING));
			services.Configure<Author>(Configuration.GetSection(nameof(Author)));

			services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
