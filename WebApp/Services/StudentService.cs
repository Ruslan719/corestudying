﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApp.DataModels;

namespace WebApp.Services
{
	/// <summary>
	/// Сервис студентов.
	/// </summary>
    public class StudentService : IStudentService
    {
		/// <summary>
		/// Хранилище студентов.
		/// </summary>
		private List<Student> _students = new List<Student>
		{
			new Student
			{
				Id = 321,
				BirthDate = new DateTime(1997, 4, 5),
				Name = "Петя",
				GroupName = "АБ-654123"
			},
			new Student
			{
				Id = 432,
				BirthDate = new DateTime(1995, 12, 23),
				Name = "Вася",
				GroupName = "ВЛ-444555"
			}
		};

		/// <summary>
		/// Генератор случайных чисел.
		/// </summary>
		private Random _random = new Random(5);

		/// <summary>
		/// Добавляет студента в хранилище.
		/// </summary>
		/// <param name="student">Добавляемый студент.</param>
		public void Add(Student student)
		{
			_students.Add(student);
		}

		/// <summary>
		/// Удаляет студента из хранилища.
		/// </summary>
		/// <param name="student_id">Идентификатор студента.</param>
		public void Remove(int student_id)
		{
			_students.RemoveAll(s => s.Id == student_id);
		}

		/// <summary>
		/// Изменяет студента.
		/// </summary>
		/// <param name="student">Измененный студент.</param>
		public void Edit(Student student)
		{
			Remove(student.Id);
			Add(student);
		}

		/// <summary>
		/// Возвращает всех студентов хранилища.
		/// </summary>
		/// <returns>Перечисление студентов.</returns>
		public IEnumerable<Student> GetAll()
		{
			return _students;
		}

		/// <summary>
		/// Возвращает студента.
		/// </summary>
		/// <param name="student_id">Идентификатор студента.</param>
		/// <returns>Студент.</returns>
		public Student Get(int student_id)
		{
			return _students.FirstOrDefault(s => s.Id == student_id);
		}

		/// <summary>
		/// Возвращает случайное название группы.
		/// </summary>
		/// <returns>Название группы.</returns>
		public string GetRandomGroupName()
		{
			return $"МГ-{_random.Next(100000, 999999)}";
		}
	}
}
