﻿using System.Collections.Generic;
using WebApp.DataModels;

namespace WebApp.Services
{
	/// <summary>
	/// Интерфейс сервиса студентов.
	/// </summary>
	public interface IStudentService
    {
		/// <summary>
		/// Добавляет студента.
		/// </summary>
		/// <param name="student">Добавляемый студент.</param>
		void Add(Student student);

		/// <summary>
		/// Удаляет студента.
		/// </summary>
		/// <param name="student_id">Идентификатор студента.</param>
		void Remove(int student_id);

		/// <summary>
		/// Изменяет студента.
		/// </summary>
		/// <param name="student">Измененный студент.</param>
		void Edit(Student student);

		/// <summary>
		/// Возвращает всех студентов.
		/// </summary>
		/// <returns>Перечисление студентов.</returns>
		IEnumerable<Student> GetAll();

		/// <summary>
		/// Возвращает студента.
		/// </summary>
		/// <param name="student_id">Идентификатор студента.</param>
		/// <returns>Студент.</returns>
		Student Get(int student_id);

		/// <summary>
		/// Возвращает случайное название группы.
		/// </summary>
		/// <returns>Название группы.</returns>
		string GetRandomGroupName();
	}
}
