﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApp.DataModels;
using WebApp.DbContexts;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Services
{
	/// <summary>
	/// Сервис студентов, работающий с контекстом БД.
	/// </summary>
	public class StudentDbService : IStudentService
	{
		/// <summary>
		/// Контекст студентов.
		/// </summary>
		private readonly StudentContext _studentContext;

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="student_context">Контекст студентов.</param>
		public StudentDbService(StudentContext student_context)
		{
			_studentContext = student_context;
		}

		/// <summary>
		/// Добавляет студента
		/// </summary>
		/// <param name="student">Добавляемый студент.</param>
		public void Add(Student student)
		{
			_studentContext.Students.Add(student);
			_studentContext.SaveChanges();
			_studentContext.Entry(student).State = EntityState.Detached;
		}

		/// <summary>
		/// Удаляет студента.
		/// </summary>
		/// <param name="student_id">Идентификатор студента.</param>
		public void Remove(int student_id)
		{
			_studentContext.Students.Remove(_studentContext.Students.Find(student_id));
			_studentContext.SaveChanges();
		}

		/// <summary>
		/// Изменяет студента.
		/// </summary>
		/// <param name="student">Измененный студент.</param>
		public void Edit(Student student)
		{
			Student context_student = _studentContext.Students.Find(student.Id);
			context_student.BirthDate = student.BirthDate;
			context_student.GroupName = student.GroupName;
			context_student.Name = student.Name;
			_studentContext.SaveChanges();
		}

		/// <summary>
		/// Возвращает всех студентов.
		/// </summary>
		/// <returns>Перечисление студентов.</returns>
		public IEnumerable<Student> GetAll()
		{
			IEnumerable<Student> students = _studentContext.Students
				.Include(s => s.Examinations);

			foreach (Student student in students)
			{
				_studentContext.Entry(student).State = EntityState.Detached;
			}

			return students;
		}

		/// <summary>
		/// Возвращает студента.
		/// </summary>
		/// <param name="student_id">Идентификатор студента.</param>
		/// <returns>Студент.</returns>
		public Student Get(int student_id)
		{
			Student student = _studentContext.Students
				.Include(s => s.Examinations)
				.First(s => s.Id == student_id);
			_studentContext.Entry(student).State = EntityState.Detached;
			return student;
		}

		/// <summary>
		/// Возвращает случайное название группы.
		/// </summary>
		/// <returns>Название группы.</returns>
		public string GetRandomGroupName()
		{
			Random _random = new Random(DateTime.Now.Second);
			return $"МГ-{_random.Next(100000, 999999)}";
		}
	}
}
