﻿using WebApp.DataModels;
using Microsoft.EntityFrameworkCore;

namespace WebApp.DbContexts
{
	/// <summary>
	/// Контекст студентов.
	/// </summary>
	public class StudentContext : DbContext
	{
		/// <summary>
		/// Студенты.
		/// </summary>
		public DbSet<Student> Students { get; set; }
		
		/// <summary>
		/// Сданные экзамены.
		/// </summary>
		public DbSet<Examination> Examinations { get; set; }

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="options">Параметры.</param>
		public StudentContext(DbContextOptions<StudentContext> options) :
			base(options) { }

		/// <summary>
		/// Обрабатывает событие создания модели.
		/// </summary>
		protected override void OnModelCreating(ModelBuilder model_builder)
		{
			model_builder.Entity<Student>().ToTable("Student");
			model_builder.Entity<Examination>().ToTable("Examination");
			model_builder.Entity<Examination>()
				.HasOne(e => e.Student)
				.WithMany(s => s.Examinations)
				.OnDelete(DeleteBehavior.Restrict);
		}
	}
}
