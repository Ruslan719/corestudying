﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Attributes
{
	/// <summary>
	/// Атрибут валидации имени.
	/// </summary>
    public class NameValidationAttribute : ValidationAttribute
    {
		/// <summary>
		/// Минимальная длина имени.
		/// </summary>
		private int _minLength;

		/// <summary>
		/// Максимальная длина имени.
		/// </summary>
		private int _maxLength;

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="min_length">Минимальная длина имени.</param>
		/// <param name="max_length">Максимальная длина имени.</param>
		public NameValidationAttribute(int min_length, int max_length)
		{
			_minLength = min_length;
			_maxLength = max_length;
		}

		/// <summary>
		/// Проверяет валидность имени.
		/// </summary>
		/// <param name="value">Имя.</param>
		/// <returns>Корректность имени.</returns>
		public override bool IsValid(object value)
		{
			string name = value as string;
			
			return !(string.IsNullOrWhiteSpace(name) || name.Length > _maxLength || name.Length < _minLength);
		}
	}
}
