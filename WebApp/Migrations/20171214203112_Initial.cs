﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WebApp.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Student",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BirthDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    GroupName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Student", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Examination",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Grade = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StudentId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Examination", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Examination_Student_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Student",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Examination_StudentId",
                table: "Examination",
                column: "StudentId");

			// Для проверки связи экзамены-студент.
			migrationBuilder.Sql(@"
insert into Student (BirthDate, GroupName, Name) 
values ('1997-4-5', 'АБ-654123', 'Anna')

insert into Student (BirthDate, GroupName, Name) 
values ('1997-12-23', 'ВЛ-444555', 'Nick')

declare @student_id1 int = (select top 1 Id from Student order by Id)
declare @student_id2 int = (select top 1 Id from Student order by Id desc)

insert into Examination (Date, Grade, Name, StudentId) 
values ('2015-01-12', 5, 'IT', @student_id1)

insert into Examination (Date, Grade, Name, StudentId) 
values ('2015-01-15', 3, 'Math', @student_id1)

insert into Examination (Date, Grade, Name, StudentId) 
values ('2015-01-12', 4, 'IT', @student_id2)");
		}

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Examination");

            migrationBuilder.DropTable(
                name: "Student");
        }
    }
}
