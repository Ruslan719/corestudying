using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace WebApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
				.ConfigureAppConfiguration((context, config) => config.AddJsonFile("studentconfig.json", false, true))
                .UseStartup<Startup>()
                .Build();
    }
}
