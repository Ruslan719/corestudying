﻿namespace WebApp.Models
{
	/// <summary>
	/// Автор проекта.
	/// </summary>
    public class Author
    {
		/// <summary>
		/// Имя.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Электронная почта.
		/// </summary>
		public string Email { get; set; }
	}
}
