﻿using System;
using System.ComponentModel.DataAnnotations;
using WebApp.Attributes;
using System.Collections.Generic;

namespace WebApp.Models
{
	/// <summary>
	/// Студент.
	/// </summary>
    public class Student
    {
		/// <summary>
		/// Идентификатор.
		/// </summary>
		[Display(Name = "Идентификатор")]
		public int? Id { get; set; }

		/// <summary>
		/// Имя.
		/// </summary>
		[Display(Name = "Имя")]
		[Required]
		[NameValidation(4, 30, ErrorMessage = "Введено некорректное имя.")]
		public string Name { get; set; }

		/// <summary>
		/// Дата рождения.
		/// </summary>
		[Display(Name = "Дата рождения")]
		[DataType(DataType.Date)]
		public DateTime BirthDate { get; set; }

		/// <summary>
		/// Название группы.
		/// </summary>
		[Display(Name = "Группа")]
		[Required]
		[RegularExpression(@"[А-Я]{1,4}-\d{6}", ErrorMessage = "Название группы не соответствует шаблону.")]
		public string GroupName { get; set; }

		/// <summary>
		/// Названия сданных экзаменов.
		/// </summary>
		[Display(Name = "Сданные экзамены")]
		public List<string> ExaminationNames { get; set; }
	}
}
